import service.CityGuideImpl;

public class Main {
    public static void main(String[] args) {

        CityGuideImpl cityGuide = new CityGuideImpl();
        cityGuide.sortAllCityByName();
        cityGuide.sortAllCityByRegionAndName();
        cityGuide.findCityMaxResidents();
        cityGuide.findCityCountInRegions();

    }
}
