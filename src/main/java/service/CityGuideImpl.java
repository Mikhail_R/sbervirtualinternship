package service;

import model.City;

import java.text.MessageFormat;
import java.util.*;

public class CityGuideImpl implements CityGuide {

    serviceReaderFile readerFile = new serviceReaderFile();
    List<City> cityListSorted = readerFile.scannerFileCity();

    @Override
    public void sortAllCityByName() {
        cityListSorted.sort(Comparator.comparing(City::getName));
        readerFile.print(cityListSorted);
    }

    @Override
    public void sortAllCityByRegionAndName() {
        cityListSorted.sort(Comparator.comparing(City::getDistrict).thenComparing(City::getName));
        readerFile.print(cityListSorted);
    }

    @Override
    public void findCityMaxResidents() {
        City maxPopulation = cityListSorted.stream()
                .max(Comparator.comparing(City::getPopulation))
                .orElseThrow();
        System.out.println(maxPopulation.getPopulation());

    }

    @Override
    public void findCityCountInRegions() {
        Map<String, Integer> collectRegions = new HashMap<>();
        cityListSorted.forEach(city -> collectRegions.merge(city.getRegion(), 1, Integer::sum));
        collectRegions.forEach((k, v) -> System.out.println(MessageFormat.format(" {0} = {1}", k, v)));

    }
}
