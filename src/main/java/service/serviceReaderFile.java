package service;

import model.City;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class serviceReaderFile {

    private final String PATH = "src/main/resources/city_ru.csv";
    List<City> cityList = new ArrayList<>();

    public List<City> scannerFileCity() {

        try (Scanner scn = new Scanner(new FileReader(PATH))) {
            while (scn.hasNextLine()) {
                cityList.add(strokeDivider(scn.nextLine()));
            }
        } catch (IOException e) {
            System.out.println("Ошибка. Не удалось прочесть файл!");
        }
        return cityList;
    }

    public void print(List<City> cityList) {
        cityList.forEach(System.out::println);
    }

    public City strokeDivider(String s) {
        Scanner scn = new Scanner(s);
        scn.useDelimiter(";");
        scn.skip("\\d*");
        String name = scn.next();
        String region = scn.next();
        String district = scn.next();
        Integer population = scn.nextInt();
        String foundation = null;
        if (scn.hasNext()) {
            foundation = scn.next();
        }
        scn.close();
        return new City(name, region, district, population, foundation);
    }
}
