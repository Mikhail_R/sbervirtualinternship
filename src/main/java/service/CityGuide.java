package service;

public interface CityGuide {

    void sortAllCityByName();
    void sortAllCityByRegionAndName();

    void findCityMaxResidents();

    void findCityCountInRegions();

}
